package ru.maxim.lesson1;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.io.IoCallback;
import io.undertow.io.Sender;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayDeque;
import java.util.Optional;

public class Lesson1
{
    public void				    runServer()
    {
	Undertow server = Undertow.builder()
	    .addHttpListener(8080, "localhost")
	    .setHandler(new BlockingHandler(new HttpHandler() 
	    {
		public void handleRequest(HttpServerExchange exchange) throws Exception 
		{
		    exchange.startBlocking();
		    String name = exchange.getQueryParameters().getOrDefault("name", new ArrayDeque<String>()).poll();
		    exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
		    try (OutputStreamWriter out = new OutputStreamWriter(exchange.getOutputStream(), "UTF-8"))
		    {
			out.write("Hello");
			out.write(" World");
			out.write(" " + name);
		    }
		    exchange.endExchange();
		}
	    })).build();
        server.start();
    }
    
    private void			    exchangeAsynchronizedVersion1(HttpServerExchange exchange)
    {
	String name = Optional.ofNullable(exchange.getQueryParameters().get("name"))
			.map(d -> d.peek())
			.orElse("defName");
	
	exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
	exchange.getResponseSender().send("Start page /, name = " + name, new IoCallback() 
	{
	    public void onComplete(HttpServerExchange hse, Sender sender) {
		hse.getResponseSender().send("onComplete");
	    }
	    public void onException(HttpServerExchange hse, Sender sender, IOException ioe) {
	    }
	});
    }
    
    class BlockingHandler implements HttpHandler
    {
	HttpHandler next;
	
	public BlockingHandler(HttpHandler next) {
	    this.next = next;
	}
	
	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
	    if (exchange.isInIoThread()) {
		exchange.dispatch(this);
		return;
	    }
	    next.handleRequest(exchange);
	}
    }
}
