package ru.maxim.lesson2;

import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.server.handlers.CookieImpl;
import io.undertow.server.handlers.form.EagerFormParsingHandler;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.server.handlers.resource.ResourceHandler;
import io.undertow.util.Headers;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

public class Questionaree
{
    private Configuration			    cfg;
    private Template				    questionTemplate;
    
    private List<Question>			    questions;
    private Deque<String>			    EMPTY_DEQUE;
    
    
    
    public void				    runServer() throws IOException
    {
	init();
	
	Undertow server = Undertow.builder()
	    .addHttpListener(8080, "localhost")
	    .setHandler(new MyHandler(Handlers.path()
		    .addPrefixPath("/", new ResourceHandler(new ClassPathResourceManager(Lesson2.class.getClassLoader(), "html")))
		    
		    .addExactPath("questionaree", new EagerFormParsingHandler().setNext((exchange) ->
		    {
			int q = 0;
			String qString = exchange.getQueryParameters().getOrDefault("q", EMPTY_DEQUE).peek();
			if (qString != null)
			    q = Integer.parseInt(qString);

			System.out.println(exchange.getRequestCookies().get("choosen"+q).getValue());

			StringWriter stringWriter = new StringWriter(); 
			questionTemplate.process(questions.get(q), stringWriter); // здесь сгенерированный html-код страницы
			exchange.getResponseSender().send(stringWriter.toString());
			    
		    }))
		    
		    .addExactPath("answer", new EagerFormParsingHandler().setNext((exchange) ->
		    {
			FormData form = exchange.getAttachment(FormDataParser.FORM_DATA);
			FormData.FormValue qfw = form.getFirst("q");
			int q = Integer.parseInt(qfw.getValue());

			FormData.FormValue answerFw = form.getFirst("answer");
			String answer = answerFw.getValue();

			Question question = questions.get(q);
			int choosenIndex = question.getAnswers().indexOf(answer);
			question.setChoosen(choosenIndex);
			boolean isRight = question.isRight(answer);

			Cookie cookie = exchange.getRequestCookies().get("rightAnswers");
			int rightAnswers = 0;
			if (cookie != null)
			    rightAnswers = Integer.parseInt(cookie.getValue());
			if (isRight)
			    rightAnswers++;


			if (q < questions.size()-1)
			{
			    exchange.getResponseCookies().put("rightAnswers", 
				    new CookieImpl("rightAnswers").setValue(rightAnswers + ""));
			    exchange.getResponseCookies().put("choosen" + q, 
				    new CookieImpl("choosen" + q).setValue(""+choosenIndex));
			    Handlers.redirect("questionaree?q=" + (q + 1)).handleRequest(exchange);
			}
			else
			{
			    if (cookie != null)
			    {
				exchange.getResponseCookies().put("rightAnswers", cookie
					.setValue("0")
					/*.setMaxAge(0)*/);
				exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
				exchange.getResponseSender().send("Rigth answers: " + rightAnswers);
			    }
			}
		    }))
		    
	    )).build();
        server.start();
    }
    
    private void			    init() throws IOException
    {
	cfg = new Configuration(Configuration.VERSION_2_3_23);
	cfg.setClassLoaderForTemplateLoading(Lesson2.class.getClassLoader(), "templates");
	cfg.setDefaultEncoding("UTF-8");
	cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
	cfg.setObjectWrapper(new BeansWrapper(Configuration.VERSION_2_3_23));
	
	questionTemplate = cfg.getTemplate("question.html");
	
	questions = Arrays.asList(
                new Question(" 2 + 2", Arrays.asList("2", "5", "4"), 2),
                new Question(" 3 + 3", Arrays.asList("6", "8", "9"), 0),
                new Question(" 1 - 1", Arrays.asList("-1", "0", "1"), 1)
        );
        for (int i = 0; i < questions.size(); i++) {
            questions.get(i).setNum(i);
        }
	
	EMPTY_DEQUE = new ArrayDeque<>(0);
    }

    
    class MyHandler implements HttpHandler
    {
	HttpHandler next;
	
	public MyHandler(HttpHandler next) {
	    this.next = next;
	}
	
	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception 
	{
	    try {
		next.handleRequest(exchange);
	    } catch (Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		exchange.setStatusCode(500);
		pw.close();
		exchange.getResponseSender().send(sw.toString());
	    }
	}
    }
}
