package ru.maxim.lesson2;

import java.util.List;

public class Question
{
    String text;
    List<String> answers;
    int right;
    int num = 0;
    int choosen = -1;

    public Question(String text, List<String> answers, int right) {
        this.text = text;
        this.answers = answers;
        this.right = right;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public int getRight() {
        return right;
    }

    public String getText() {
        return text;
    }

    public int getNum() {
        return num;
    }
    
    public int getChoosen() {
	return choosen;
    }
    public void setChoosen(int choosen) {
	this.choosen = choosen;
    }

    public void setNum(int num) {
        this.num = num;
    }
    
    public boolean isRight(String answer) {
	String r = getAnswers().get(right);
	return r.equals(answer);
    }
}
