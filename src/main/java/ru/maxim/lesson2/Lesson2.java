package ru.maxim.lesson2;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.EagerFormParsingHandler;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.server.handlers.resource.ResourceHandler;
import io.undertow.util.AttachmentKey;
import io.undertow.util.Headers;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;

public class Lesson2
{
    static final AttachmentKey<String>		    myString = AttachmentKey.create(String.class);
    
    
    
    public void				    runServer() throws IOException
    {
	Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
	cfg.setClassLoaderForTemplateLoading(Lesson2.class.getClassLoader(), "templates");
	cfg.setDefaultEncoding("UTF-8");
	cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
	
	Template nameTemplate = cfg.getTemplate("name.html");
	Template questionTemplate = cfg.getTemplate("question.html");
	
	
	Undertow server = Undertow.builder()
	    .addHttpListener(8080, "localhost")
	    .setHandler(Handlers.path()
		    .addPrefixPath("/", new ResourceHandler(new ClassPathResourceManager(Lesson2.class.getClassLoader(), "html")))
		    
		    .addExactPath("form", new EagerFormParsingHandler().setNext((exchange) -> 
		    {
			exchange.putAttachment(myString, "KEK");
			
			FormData form = exchange.getAttachment(FormDataParser.FORM_DATA);
			FormData.FormValue firstFv = form.getFirst("name");
			String name = firstFv.getValue();
			
			exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");	
			
			HashMap<String, Object> templateData = new HashMap<String, Object>();
			templateData.put("name", name);
			StringWriter stringWriter = new StringWriter();
			nameTemplate.process(templateData, stringWriter);

			exchange.getResponseSender().send(stringWriter.toString());                             
		    }))
		    
		    .addExactPath("question", null)
		    
	    ).build();
        server.start();
    }

    class BlockingHandler implements HttpHandler
    {
	HttpHandler next;
	
	public BlockingHandler(HttpHandler next) {
	    this.next = next;
	}
	
	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
	    if (exchange.isInIoThread()) {
		exchange.dispatch(this);
		return;
	    }
	    next.handleRequest(exchange);
	}
    }
}
